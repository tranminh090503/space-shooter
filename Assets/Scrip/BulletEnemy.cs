using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletEnemy : MonoBehaviour
{
    [SerializeField] private GameObject m_EnemyShooter;


    
    void Start()
    {

        StartCoroutine(EnemyShoot());
    }

    void LateUpdate()
    {


    }
    IEnumerator EnemyShoot()
    {
        yield return new WaitForSeconds(1f);
        Vector3 Pos = transform.position;
        GameObject bullet = Instantiate(m_EnemyShooter, Pos, Quaternion.identity);
        Destroy(bullet, 4f);
        StartCoroutine(EnemyShoot());
       
    }
    
}
