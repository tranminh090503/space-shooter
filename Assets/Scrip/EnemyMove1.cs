using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMove1 : MonoBehaviour
{
    public EnemyMove1 Move1;
    [SerializeField] float speedX;
    [SerializeField] float speedY;
    public GameObject target1;
    
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
            transform.position += new Vector3(Random.Range(0.01f,0.05f), Random.Range(0.01f,0.05f), 0) ;
        transform.position = Vector3.Lerp(transform.position, target1.transform.position, 0.01f);
    }
}
