using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMove2 : MonoBehaviour
{
    [SerializeField]
    GameObject[] targets;
    private int m_currentTargetIndex;
    [SerializeField] float speedX;
    [SerializeField] float speedY;
    GameObject m_currentTarget;
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

        m_currentTarget = targets[m_currentTargetIndex];
        if(Vector3.Distance(transform.position, m_currentTarget.transform.position) < 0.1f) 
        {
            m_currentTargetIndex++;
            
            if(m_currentTargetIndex > targets.Length - 1) 
            {

                m_currentTargetIndex = 0;
            }
            m_currentTarget = targets[m_currentTargetIndex];


        }
        transform.position = Vector3.Lerp(transform.position, m_currentTarget.transform.position, 0.01f);
    }
}
