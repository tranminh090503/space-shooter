using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemySpawn1 : MonoBehaviour
{
    public Player player;
    public EnemyMove1 Move1;
    [SerializeField] float timeDelay;
    private float timeStart;
    [SerializeField] GameObject _enemy;
   
    void Start()
    {
        timeStart = 2f;
    }

    // Update is called once per frame
    void Update()
    {
        timeDelay -= Time.deltaTime;
        if (timeDelay < 0)
        {

            GameObject _enemyFake = Instantiate(_enemy, new Vector3(-7.5f, 0, 0), Quaternion.identity);
           EnemyMove1 enemyMove1 = _enemyFake.GetComponent<EnemyMove1>();
            enemyMove1.target1 = player.gameObject;
            enemyMove1.Move1 = Move1;
            Destroy(_enemyFake, 10f);
            timeDelay = timeStart;



        }
        //transform.position = Vector3.Lerp(transform.position, target1.transform.position, 0.01f);
    }
}
