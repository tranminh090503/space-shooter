using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawn2 : MonoBehaviour
{
    public Player player;
    [SerializeField] float timeDelay;
    private float timeStart;
    [SerializeField] GameObject _enemy;
    public EnemyMove1 Move2;
    void Start()
    {
        timeStart = timeDelay;
    }

    void Update()
    {
        timeDelay -= Time.deltaTime;
        if (timeDelay < 0)
        {
            GameObject _enemyFake = Instantiate(_enemy, new Vector3(6.96f, Random.Range(-6.40f, 6f), 0), Quaternion.identity);
            Destroy(_enemyFake, 10f);
            EnemyMove2 enemyMove2 = _enemy.GetComponent<EnemyMove2>();
            
            timeDelay = timeStart;
        }

    }
}
