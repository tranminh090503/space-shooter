using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hp : MonoBehaviour
{
    public EnemySpawn1 EnemySpawn1;
    [SerializeField] int hp;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (gameObject == null)
        {
            Time.timeScale = 0;
           
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        hp--;
        Destroy(collision.gameObject);
        if (hp < 0)
        {
            Destroy(gameObject);
            Time.timeScale = 0;

        }
        
    }
}
