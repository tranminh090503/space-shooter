using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Lazer : MonoBehaviour
{
    
    private float m_Scountdown;
    [SerializeField] private float m_timeDuration;
    
    public GameObject lazer;
    public Player player1;
    
     //public Player player;

    void Start()
    {
      
        m_Scountdown = 1f;
    }
    // Update is called once per frame
    void Update()
    {
        this.Shoot();
       
    }
    void Shoot()
    {
        m_Scountdown -= Time.deltaTime;
        if(m_Scountdown <= 0f) 
        {
            Vector3 spawnPos = transform.position;
            GameObject m_lazer = Instantiate(lazer, spawnPos  , Quaternion.identity);
            Destroy(m_lazer, 8f);
            m_Scountdown = m_timeDuration;
            
            LazerMove layzerMver = m_lazer.GetComponent<LazerMove>();
            layzerMver.player1 = player1;
        
            player1.scoreText.text = player1.Score.ToString();
     
       

        }
        
    }
   
}
