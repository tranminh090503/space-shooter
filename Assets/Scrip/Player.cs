﻿using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEditor.Timeline;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
    [SerializeField] float moveSpeed = 5f;
    [SerializeField] float paddingLeft;
    [SerializeField] float paddingRight;
    [SerializeField] float paddingTop;
    [SerializeField] float paddingBottom;
    [SerializeField] Vector3 worldPossition;
    [SerializeField] int hp;

    public int Score = 0;
    public Text scoreText;

    public float rawInput;
    Vector2 minBounds;
    Vector2 maxBounds;


    private void Start()
    {
        Score = 0;
        scoreText.text = Score.ToString();
        InitBounds();
    }
    void Update()
    {
        Move();

    }
    private void InitBounds()
    {
        Camera mainCamera = Camera.main;
        minBounds = mainCamera.ViewportToWorldPoint(new Vector2(0, 0));
        maxBounds = mainCamera.ViewportToWorldPoint(new Vector2(1, 1));
    }
    private void Move()
    {




        Vector2 newPos = new Vector2();
        newPos.x = Mathf.Clamp(transform.position.x, minBounds.x + paddingLeft, maxBounds.x - paddingRight);
        newPos.y = Mathf.Clamp(transform.position.y, minBounds.y + paddingBottom, maxBounds.y - paddingTop);

        transform.position = newPos;
    }

    private void FixedUpdate()
    {
        if (Input.GetMouseButton(0))
        {

            this.worldPossition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            this.worldPossition.z = 0;
            Vector3 newPos = Vector3.Lerp(transform.position, this.worldPossition, this.moveSpeed);
            transform.position = newPos;

            transform.rotation = Quaternion.Euler(0, 45f, 0);

        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {


        Debug.Log("aaaaaaa");
        Score += 1;

        scoreText.text = Score.ToString();

        


    }
   
}
